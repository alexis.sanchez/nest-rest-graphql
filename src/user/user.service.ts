import { Injectable } from '@nestjs/common';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {
  constructor(private repository: UserRepository) {}

  async create(data: CreateUserInput) {
    return await this.repository.createUser({
      data,
    });
  }

  async findAll() {
    return await this.repository.getUsers({});
  }

  async findOne(id: number) {
    return await this.repository.getOneUser({ where: { id } });
  }

  async update(id: number, updateTaskDto: UpdateUserInput) {
    return this.repository.updateUser({
      where: { id },
      data: updateTaskDto,
    });
  }

  async remove(id: number) {
    const exist = await this.findOne(id);
    if (exist?.id) {
      await this.repository.deleteUser({ where: { id } });
      return `This action removes a #${id} user`;
    } else {
      return `#${id} user, not found`;
    }
  }
}
