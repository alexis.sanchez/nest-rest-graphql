import { ObjectType, Field, Int, GraphQLISODateTime } from '@nestjs/graphql';
import { User as prismaDB } from '@prisma/client';
import { Task } from 'src/task/entities/task.entity';

@ObjectType()
export class User {
  @Field(() => Int, { description: 'id user' })
  id: prismaDB[`id`];

  @Field(() => GraphQLISODateTime)
  createdAt: prismaDB[`createdAt`];

  @Field(() => GraphQLISODateTime)
  updatedAt: prismaDB[`updatedAt`];

  @Field(() => String)
  email: string;

  @Field(() => String)
  name?: string;

  @Field(() => [Task], { nullable: true })
  tasks?: Task[];
}
