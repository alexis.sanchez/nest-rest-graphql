import { Field, InputType } from '@nestjs/graphql';
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator';

@InputType()
export class CreateUserInput {
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  @Field(() => String, { description: 'email user' })
  email: string;

  @IsString()
  @MinLength(2)
  @IsOptional()
  @Field(() => String, { description: 'name user' })
  name?: string;
}
