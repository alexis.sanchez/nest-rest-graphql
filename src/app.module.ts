import { Module } from '@nestjs/common';
import { CommomModule } from './common/common.module';
import { TaskModule } from './task/task.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [CommomModule, UserModule, TaskModule],
})
export class AppModule {}
