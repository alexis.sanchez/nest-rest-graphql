import { Injectable } from '@nestjs/common';
import { TaskStatus } from './entities/task.entity';
import { CreateTaskInput } from './dto/create-task.input';
import { UpdateTaskInput } from './dto/update-task.input';
import { TaskRepository } from './task.repository';

@Injectable()
export class TaskService {
  constructor(private repository: TaskRepository) {}

  async create({ title, description, authorId }: CreateTaskInput) {
    return await this.repository.createTask({
      data: {
        title,
        description,
        status: TaskStatus.pending,
        author: { connect: { id: authorId } },
      },
    });
  }

  async findAll() {
    return await this.repository.getTasks({});
  }

  async findOne(id: number) {
    return await this.repository.getOneTask({ where: { id } });
  }

  async update(id: number, updateTaskDto: UpdateTaskInput) {
    return this.repository.updateTask({
      where: { id },
      data: updateTaskDto,
    });
  }

  async remove(id: number) {
    const exist = await this.findOne(id);
    if (exist?.id) {
      await this.repository.deleteTask({ where: { id } });
      return `This action removes a #${id} task`;
    } else {
      return `#${id} task, not found`;
    }
  }
}
