import {
  Field,
  GraphQLISODateTime,
  Int,
  ObjectType,
  registerEnumType,
} from '@nestjs/graphql';
import { Task as prismaDB } from '@prisma/client';
import { User } from 'src/user/entities/user.entity';

export enum TaskStatus {
  completed = 'COMPLETED',
  pending = 'PENDING',
}

registerEnumType(TaskStatus, {
  name: 'TaskStatus',
  description: 'Status task',
});

@ObjectType()
export class Task {
  @Field(() => Int, { description: 'id task' })
  id: prismaDB[`id`];

  @Field(() => GraphQLISODateTime)
  createdAt: prismaDB[`createdAt`];

  @Field(() => GraphQLISODateTime)
  updatedAt: prismaDB[`updatedAt`];

  @Field(() => String, { description: 'Title' })
  title: prismaDB[`title`];

  @Field(() => String, { description: 'Description' })
  description: prismaDB[`description`];

  @Field(() => TaskStatus, { description: 'Status (COMPLETED, PENDING)' })
  status: prismaDB[`status`];

  @Field(() => User, { description: 'author data' })
  author?: User;
}
