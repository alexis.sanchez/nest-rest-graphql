import { Field, InputType } from '@nestjs/graphql';
import { IsInt, IsNotEmpty, IsString, MinLength } from 'class-validator';

@InputType()
export class CreateTaskInput {
  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @Field(() => String, { description: 'Title task' })
  title: string;

  @IsNotEmpty()
  @IsString()
  @Field(() => String, { description: 'Description task' })
  description: string;

  @IsNotEmpty()
  @IsInt()
  @Field(() => Number, { description: 'Author id' })
  authorId: number;
}
