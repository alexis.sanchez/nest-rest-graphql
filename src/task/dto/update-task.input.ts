import { CreateTaskInput } from './create-task.input';
import { InputType, Field, PartialType } from '@nestjs/graphql';
import { IsEnum, IsOptional } from 'class-validator';
import { TaskStatus } from '../entities/task.entity';

@InputType()
export class UpdateTaskInput extends PartialType(CreateTaskInput) {
  @Field(() => TaskStatus, { description: 'Status task', nullable: true })
  @IsEnum(TaskStatus)
  @IsOptional()
  status?: TaskStatus;
}
