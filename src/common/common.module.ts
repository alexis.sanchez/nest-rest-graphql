import { Module } from '@nestjs/common';
import { GraphqlModule } from 'src/common/graphql.module';

@Module({
  imports: [GraphqlModule],
  exports: [GraphqlModule],
})
export class CommomModule {}
